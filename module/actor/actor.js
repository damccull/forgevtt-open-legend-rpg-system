/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class OpenLegendRPGActor extends Actor {

  /**
   * Augment the basic actor data with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

    const actorData = this.data;
    const data = actorData.data;
    const flags = actorData.flags;

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    if (actorData.type === 'character') this._prepareCharacterData(actorData);
    if (actorData.type === 'npc') this._prepareNpcData(actorData);
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const data = actorData.data;

    // Make modifications to data here. For example:

    var fortitude = data.attributes.physical.fortitude;
    var presence = data.attributes.social.presence;
    var will = data.attributes.mental.will;
    var agility = data.attributes.physical.agility;
    var might = data.attributes.physical.might;
    //var armor = data.;

    data.stats.hp.max = 10 + (2 * (fortitude + presence + will));
    data.stats.hp.label = "Health";
    data.stats.defense.toughness.value = 10 + fortitude + will;
    data.stats.defense.toughness.label = "Toughness";
    // TODO: figure out how to add character's armor to 'guard'. Maybe in another place?
    data.stats.defense.guard.value = 10 + agility + might;
    data.stats.defense.guard.label = "Guard";
    data.stats.defense.resolve.value = 10 + presence + will;
    data.stats.defense.resolve.label = "Resolve";

    // Loop through ability scores, and add their modifiers to our sheet output.
    // for (let [key, ability] of Object.entries(data.abilities)) {
    //   // Calculate the modifier using d20 rules.
    //   ability.mod = Math.floor((ability.value - 10) / 2);
    // }
  }

  _prepareNpcData(actorData) {
    const data = actorData.data;

    // Make modifications to data here. For example:

    // Loop through ability scores, and add their modifiers to our sheet output.
    // for (let [key, ability] of Object.entries(data.abilities)) {
    //   // Calculate the modifier using d20 rules.
    //   ability.mod = Math.floor((ability.value - 10) / 2);
    // }
  }

}